# static

This will deploy any file/folder located in the `public/` folder to s3.
They will be available under `https://love2sign4you-dev.s3-eu-west-1.amazonaws.com/public/`
